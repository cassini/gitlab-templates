# SPDX-FileCopyrightText: <text>Copyright 2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

"""
This script checks for successful CI test results in a GitLab pipeline.

The script performs the following steps:
1. Reads a JSON file containing the list of required tests.
2. Fetches the test reports from a specified GitLab pipeline.
3. Extracts the names of successful test cases from the test reports.
4. Compares the fetched test results with the expected test IDs.
5. Prints a message indicating whether all expected tests passed or
if any tests are missing or failing.

Usage:
    python test_check.py <file_path>

Args:
    file_path (str): Path to the JSON file containing
                     the list of required tests.

Environment Variables:
    CI_BOT_API_TOKEN (str): The private API token for GitLab authentication.
    CI_PIPELINE_ID (str): The ID of the pipeline to fetch results from.
    CI_PROJECT_ID (str): The ID of the project containing the pipeline.
    CI_SERVER_PROTOCOL (str): The protocol of the GitLab server.
    CI_SERVER_HOST (str): The host of the GitLab server.

Functions:
    get_expected_test_ids(file_path: str) -> Set[str]:
        Reads a JSON file and returns a set of concatenated key-value strings.

    get_success_test_ids(test_report) -> Set[str]:
        Extracts the names of successful test cases from a test report.

    fetch_test_reports(url: str, api_token: str,
                       pipeline_id: str, project_id: str) -> list:
        Fetches the test reports from a GitLab pipeline.

    create_logger(name: str) -> logging.Logger
        Creates and configures a logger with handlers for stdout and stderr.

    def main() -> None:
        The main function that parses arguments,
        reads expected test IDs, fetches actual test results,
        and compares them to determine if all expected tests passed.
"""

import os
import sys
import json
import logging
import argparse
from enum import Enum
from typing import Set, Any
import gitlab


class ReturnCode(Enum):
    """Return codes."""

    SUCCESS = 0
    ERROR = 1


def get_expected_test_ids(tests_description_json_file_path: str) -> Set[str]:
    """
    Reads a JSON file from the specified file path
    and returns a set of expected test IDs.

    Each string in the set is formed by concatenating the key and each value
    from the JSON file, separated by a dot.

    Args:
        tests_description_json_file_path (str): The path to the JSON file.

    Returns:
        Set[str]: A set of expected test IDs.
    """
    with open(tests_description_json_file_path,
              'r', encoding='utf-8') as file:
        expected_test_ids = set()
        data = json.load(file)
        for test_suite, test_names in data.items():
            for test_name in test_names:
                expected_test_ids.add(f"{test_suite}.{test_name}")
        return expected_test_ids


def fetch_test_reports(url: str, api_token: str,
                       pipeline_id: str, project_id: str) -> list:
    """
    Fetches the test reports from a GitLab pipeline.

    Connects to the GitLab API using the provided URL and API token,
    retrieves the specified pipeline, and iterates through its bridges
    to find downstream pipelines. Extracts test reports from these pipelines.

    Args:
        url (str): The GitLab instance URL.
        api_token (str): The private API token for authentication.
        pipeline_id (str): The ID of the pipeline to fetch results from.
        project_id (str): The ID of the project containing the pipeline.

    Returns:
        list: A list of test reports from the downstream pipelines.
    """
    test_reports = []
    gl = gitlab.Gitlab(url, private_token=api_token)
    project = gl.projects.get(project_id)
    pipeline = project.pipelines.get(pipeline_id)
    for bridge in pipeline.bridges.list():
        if bridge.downstream_pipeline is not None:
            pipeline = project.pipelines.get(
                bridge.downstream_pipeline['id']
            )
    test_report = pipeline.test_report.get()
    test_reports.append(test_report)
    return test_reports


def get_success_test_ids(test_report) -> Set[str]:
    """
    Extracts the names of successful test cases from a test report.

    Iterates through the test suites and test cases in the test report,
    adding the names of successful test cases to a set.

    Args:
        test_report: The test report object containing
        test suites and test cases.

    Returns:
        Set[str]: A set of names of successful test cases.
    """
    success_tests = set()
    for test_suite in test_report.test_suites:
        for test_case in test_suite["test_cases"]:
            if test_case["status"] == "success":
                success_tests.add(f"{test_suite['name']}.{test_case['name']}")
    return success_tests


def create_logger(name: str) -> logging.Logger:
    """
    Create a logger with handlers for stdout and stderr.

    Args:
        name (str): The name of the logger.

    Returns:
        logging.Logger: Configured logger instance.
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    stdout_handler = logging.StreamHandler(sys.stdout)
    stderr_handler = logging.StreamHandler(sys.stderr)

    stdout_handler.setLevel(logging.INFO)
    stderr_handler.setLevel(logging.ERROR)

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stdout_handler.setFormatter(formatter)
    stderr_handler.setFormatter(formatter)

    logger.addHandler(stdout_handler)
    logger.addHandler(stderr_handler)

    return logger


def main() -> None:
    """
    The main function that parses command-line arguments,
    reads expected test IDs from a JSON file,
    fetches actual test results from a GitLab pipeline,
    and compares them to determine if all expected tests passed.

    It performs the following steps:
    1. Parses the command-line argument to get the file path of the JSON file
    containing the list of expected tests.
    2. Reads the expected test IDs from the JSON file.
    3. Retrieves necessary environment variables for GitLab
    API authentication and pipeline details.
    4. Create a logger with handlers for stdout and stderr
    5. Fetches test reports from the specified GitLab pipeline.
    6. Extracts the names of successful test cases from the test reports.
    7. Compares the fetched test results with the expected test IDs.
    8. Prints a message indicating whether all expected tests passed or
    if any tests are missing or failing.
    9. Exits with a status code of 0 if all tests passed,
    or 1 if any tests are missing or failing.
    """
    parser = argparse.ArgumentParser(
        description="Python script for CI test checking")
    parser.add_argument("file_path", type=str,
                        help="Path(Relative or Absolute) to JSON file"
                             "containing the list of expected tests")
    args = parser.parse_args()

    api_token = os.getenv('CI_BOT_API_TOKEN')
    pipeline_id = os.getenv('CI_PIPELINE_ID')
    project_id = os.getenv('CI_PROJECT_ID')
    logger = create_logger(__name__)

    if not api_token or not pipeline_id or not project_id:
        logger.error("Environment variables CI_BOT_API_TOKEN, CI_PIPELINE_ID,"
                     "and CI_PROJECT_ID must be set")
        sys.exit(ReturnCode.ERROR.value)

    url = f"{os.getenv('CI_SERVER_PROTOCOL')}://{os.getenv('CI_SERVER_HOST')}"

    try:
        test_reports = fetch_test_reports(
            url, api_token, pipeline_id, project_id)
        successful_test_ids = set()
        for test_report in test_reports:
            successful_test_ids.update(get_success_test_ids(test_report))
    except gitlab.exceptions.GitlabGetError as e:
        logger.error(f"Error fetching test results: {e}")
        sys.exit(ReturnCode.ERROR.value)

    if not successful_test_ids:
        logger.error(f"Cannot fetch test reports"
                     f"from the pipeline ID:{pipeline_id}")
        sys.exit(ReturnCode.ERROR.value)

    expected_test_ids = get_expected_test_ids(args.file_path)
    unsuccessful_test_ids = expected_test_ids.difference(successful_test_ids)

    if unsuccessful_test_ids:
        logger.error(f"FAIL: Missing or failing tests:"
                     f"{', '.join(unsuccessful_test_ids)}")
        sys.exit(ReturnCode.ERROR.value)
    else:
        logging.info("PASS")
        sys.exit(ReturnCode.SUCCESS.value)


if __name__ == "__main__":
    main()
