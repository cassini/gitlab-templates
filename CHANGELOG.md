## 1.6.4 (2024-07-24)

### bug (1 change)

- [lava: TuxSuite: Fix download_url](https://gitlab.com/Linaro/cassini/gitlab-templates/-/commit/6cfe0970d080e2027cc7f9c0242ba73ee36ef4b8)

### other (1 change)

- [[yocto, kas] Hide dangerous jobs when using widely shared caches](https://gitlab.com/Linaro/cassini/gitlab-templates/-/commit/86ee021ad96e2ab03f42c13d2c832fd4ab7f2add) ([merge request](https://gitlab.com/Linaro/cassini/gitlab-templates/-/merge_requests/20))

## 1.6.3 (2023-12-15)

### bug (1 change)

- [lava: TuxSuite: Fix headers to curl command](Linaro/cassini/gitlab-templates@c742d64c7e91323635ccd762fcac6d27b67bc110) ([merge request](Linaro/cassini/gitlab-templates!14))

## 1.6.2 (2023-12-13)

### other (1 change)

- [Bump version number to v1.6.2](Linaro/cassini/gitlab-templates@daf4b40e4a6893da92dc3acb96aadacde4503b04)

## 1.6.1 (2023-12-07)

### bug (1 change)

- [lava: Correct regex logic](Linaro/cassini/gitlab-templates@9a585a92136671db8435675454eb1b118d890281)

## 1.6.0 (2023-11-29)

### feature (1 change)

- [lava: Enable Tuxsuite test usage](Linaro/cassini/gitlab-templates@587ac357c66291b24b3bde42764f1953bffb9305)

## 1.5.4 (2023-10-24)

### bug (1 change)

- [child: Ensure parent pipeline source is available within the child](Linaro/cassini/gitlab-templates@4f26b5493a7a76cc4d50d4cbd0226858417dab3b)

## 1.5.3 (2023-09-18)

### bug (1 change)

- [danger: Fetch complete git history](Linaro/cassini/gitlab-templates@12c182f43dc3e60254fda3ce5d2f7cdfc629c109)

## 1.5.2 (2023-08-30)

### bug (1 change)

- [docker: Move to v1.31.0 of the buildah container](Linaro/cassini/gitlab-templates@347d50d361b9e67a2ec40e5a8f5c336d4a15fb58)

## 1.5.1 (2023-07-18)

### bug (1 change)

- [ci: Set Git strategy to do a full cloning](Linaro/cassini/gitlab-templates@56d6e9a53206f4de6b87f0cee3197b7939ed5ca4) by @ZiadElhanafy

## 1.4.3 (2023-06-02)

### bug (1 change)

- [common: No longer need to find the project ID](cassini/gitlab-templates@98ce920634c9d46837eddc1792d1dbb446966607)

## 1.4.2 (2023-05-11)

### bug (1 change)

- [Make entrypoint handling consistent](cassini/gitlab-templates@1447b0878aab894c198e408998f22bf812df8f8a)

## 1.4.1 (2023-05-02)

### bug (1 change)

- [[lava, child] Use CI_REGISTRY_IMAGE in image paths](cassini/gitlab-templates@fc3dac89af4c0e21a4ee2314ba1494537ca67226)

## 1.4.0 (2023-04-25)

### feature (1 change)

- [static-analysis: Add in the use of the cspell checker](cassini/gitlab-templates@070f2988265fb1b7bc948f794f574c08dfb424c1)

## 1.3.0 (2023-04-19)

### other (1 change)

- [ci: Enable the python checkers](cassini/gitlab-templates@62d5891024cdc9ea34eccedee4ed478cd9208539)

### feature (1 change)

- [static-analysis: Add in the use of the clang-format checker](cassini/gitlab-templates@da2476504e908802c6a2cb1b017468fc1ad5732d)

## 1.2.0 (2023-04-12)

### feature (4 changes)

- [common: Retry by default](cassini/gitlab-templates@e30df0f887070f1ade9fd0264d3d9aa6b51911a7)
- [static-analysis: Add in the use of the cmakelint checker](cassini/gitlab-templates@1980397d91ee914cd8b3078589246fa44ec6293b)
- [static-analysis: Add in the use of the oelint-adv checker](cassini/gitlab-templates@e551c28c87776f8b35012dc762128d65c80ccce7)
- [static-analysis: Enable secret detection](cassini/gitlab-templates@9e27f272e2512e45c9dd9025ea360c349a6422d1)

### other (1 change)

- [Switch to expecting format-1 copyright notices](cassini/gitlab-templates@9e33772cd4013b18f6d99ea299af6ec3e792b0f4)

### bug (1 change)

- [common: Correct the tag used on the default job definition](cassini/gitlab-templates@30928929e7088e8ca216c4d206b1501cc569eade)

## 1.1.2 (2023-03-09)

### other (1 change)

- [common: Support to run from a multi-project pipeline](cassini/gitlab-templates@690b6b869271ae9f67fe8d5e37d39092a4934122)

## 1.1.1 (2023-01-26)

### bug (1 change)

- [docker: Need to push the image after building](cassini/gitlab-templates@e9c0f57fb0678ab5cd7d7f0023805914212a0913)

## 1.1.0 (2023-01-25)

### feature (1 change)

- [static-analysis: Add in the use of the inclusivity checker](cassini/gitlab-templates@7dd03452ad8508490c7f29e0f4a0c237c49788e2)

## 1.0.0 (2023-01-24)

### feature (2 changes)

- [docker: Switch to using buildah for docker builds](cassini/gitlab-templates@9b547bdeaf11f021317e9271644c0ff7acef3714)
- [common: New job template for fetch files](cassini/gitlab-templates@3c8582216d8403207d3bd9a612c30db7f9ff0dbd) ([merge request](cassini/gitlab-templates!17))

### other (1 change)

- [lava: Error when job submission fails](cassini/gitlab-templates@e3f44489e0683ef14d0e15a6c4104ef92b06d3af) ([merge request](cassini/gitlab-templates!17))

## 0.0.13 (2022-12-12)

### bug (1 change)

- [static-analysis: Pull latest tag of plugins](cassini/gitlab-templates@46656fdb55fa0bbf4339245588e2af7f6823481a) ([merge request](cassini/gitlab-templates!15))

## 0.0.12 (2022-11-11)

### feature (2 changes)

- [Child pipeline helpers](engineering/embedded-a/templates@2872d98eccbaccf288737d45edb65f3f8c2a9bb0)
- [Add support for fetching version number from json files](engineering/embedded-a/templates@584466404b484d3e31d174680b47832ddf66069e)

### other (1 change)

- [Add changelog generation and releasing to ci](engineering/embedded-a/templates@a98e830e5f95150131beb4151843702731252d50)

## 0.0.11 (2022-11-11)

### other (1 change)

- [Add changelog generation and releasing to ci](engineering/embedded-a/templates@a98e830e5f95150131beb4151843702731252d50)

### feature (2 changes)

- [Add support for fetching version number from json files](engineering/embedded-a/templates@584466404b484d3e31d174680b47832ddf66069e)
- [Add yocto and kas build templates](engineering/embedded-a/templates@f69651997ec05c143c9be9d2eaa6eefede082083) ([merge request](engineering/embedded-a/templates!12))
